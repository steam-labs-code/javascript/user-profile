/**
 * User Profile Page
 *
 * All objects within this script are accessible in your browser via the console. Simply open index.html in your browser and try it. For example, open your console and type 'smokeTest' to see its value.
 */
const smokeTest = 'Proof that this is testable via the browser console!';

// TASK: Create variables for the relevant HTML elements
const form = document.querySelector('form');
const formButton = document.querySelector('#formButton');
const profileForm = document.querySelector('#profileForm');
const successMsg = document.querySelector('#message');
let fullName = document.querySelector('#profileName').textContent;
let contactInfo = document.querySelectorAll('.contactInfo li');
let userProfile = {};

// TASK: Split the address into 4 parts: address, city, state, zip
let [city, stateZip] = contactInfo[1].textContent.split(',');
let [_, state, zipcode] = stateZip.split(' ');

// TASK: Build the user object
userProfile.fullname = fullName;
userProfile.address = contactInfo[0].textContent;
userProfile.city = city;
userProfile.state = state;
userProfile.zipcode = zipcode;
userProfile.phone = contactInfo[2].textContent;
userProfile.email = contactInfo[3].textContent;

// TASK: Create variables to access form input values
let addressInput = document.querySelector('#addressInput');
let cityInput = document.querySelector('#cityInput');
let stateInput = document.querySelector('#stateInput');
let zipInput = document.querySelector('#zipInput');
let emailInput = document.querySelector('#emailInput');
let phoneInput = document.querySelector('#phoneInput');

// TASK: Try adding an event listener to the formButton instead to see what happens.
form.addEventListener('submit', updateProfile);

function updateProfile() {
    userProfile.address = contactInfo[0].innerHTML = addressInput.value;
    userProfile.city = contactInfo[1].innerHTML = `${cityInput.value}, ${stateInput.value} ${zipInput.value}`;
    userProfile.state = stateInput.value;
    userProfile.zipcode = zipInput.value;
    userProfile.phone = contactInfo[2].innerHTML = phoneInput.value;
    userProfile.email = contactInfo[3].innerHTML = emailInput.value;

    console.log('Profile updated: ', userProfile);
    // Form and success message visibility
    profileForm.classList.remove('show');
    successMsg.classList.remove('visually-hidden')
}

function clearForm() {
    form.reset()
}
