# Web Fundamentals Practice

## Instructions

1. Open `index.html` in your browser.
2. Open `script.js` in your editor.
3. Fix the script by accessing the DOM to update the user's profile on form submission.

Experiment with the code, especially the variables, to understand how they work.

> **Rember**: Change 1 line of code at a time, meticulously observing all changes to the webpage.
